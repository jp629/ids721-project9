import streamlit as st
from transformers import AutoModelForCausalLM, AutoTokenizer
import torch
# Load the model and tokenizer for the chatbot
tokenizer = AutoTokenizer.from_pretrained("microsoft/DialoGPT-medium")
model = AutoModelForCausalLM.from_pretrained("microsoft/DialoGPT-medium")

def get_model_response(message):
    # Encode the user's input and generate a response
    input_ids = tokenizer.encode(message + tokenizer.eos_token, return_tensors='pt')
    chat_history_ids = model.generate(input_ids, max_length=1000, pad_token_id=tokenizer.eos_token_id)
    response = tokenizer.decode(chat_history_ids[:, input_ids.shape[-1]:][0], skip_special_tokens=True)
    return response

# Streamlit user interface
st.title('My Hugging Face Chatbot')
user_input = st.text_input("Say something to the chatbot:")
if user_input:
    chat_response = get_model_response(user_input)
    st.text(chat_response)